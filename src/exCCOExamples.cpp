//============================================================================
// Name        : exCCOExecuteTreeGenerator.cpp
// Author      : Gonzalo Daniel Maso Talou
// Version     :
// Copyright   :
// Description : Execution examples using the VItA library.
//============================================================================

#include <io/VTKConverter.h>
#include <stats/manipulators/StdStructStatManipulator.h>
#include <stats/ObjectTreeStatsManager.h>
#include <iostream>
#include <vector>
#include <string>

#include "io/CSVWriter.h"
#include "io/VTKObjectTreeNodalWriter.h"
#include "io/task/VisualizationSavingTask.h"

#include "structures/tree/FRRCCOSTree.h"
#include "structures/tree/SingleVesselCCOOTree.h"
#include "structures/tree/VolumetricCostEstimator.h"
#include "structures/tree/AdimSproutingVolumetricCostEstimator.h"

#include "structures/domain/AbstractDomain.h"
#include "structures/domain/SimpleDomain.h"
#include "structures/domain/DomainNVR.h"
#include "structures/domain/SimpleDomain2D.h"
#include "structures/domain/StagedDomain.h"
#include "structures/domain/UniformDistributionGenerator.h"
#include "structures/domain/NormalDistributionGenerator.h"
#include "structures/domain/CompositeDistributionGenerator.h"

#include "constrains/potentialLaw.h"
#include "constrains/bloodViscosity.h"
#include "constrains/symmetryLaw.h"

#include "core/StagedFixedPerfusionRadiusTreeGenerator.h"
#include "core/StagedFRROTreeGenerator.h"
#include "core/FixedPerfusionRadiusTreeGenerator.h"
#include "core/GeneratorData.h"

#include "constrains/AbstractConstraintFunction.h"
#include "constrains/ConstantConstraintFunction.h"
#include "constrains/ConstantPiecewiseConstraintFunction.h"

#include "creators/CylinderCreator.h"
#include "creators/SphereCreator.h"

using namespace std;

string inputFolder = "/hpc/gmas685/workspaces/eclipse-workspace/exCCOPaperExamples/inputs/";
string outputFolder = "/hpc/gmas685/workspaces/eclipse-workspace/exCCOPaperExamples/outputs/";

void stagedGrowingExampleParameters1Stage(int terminals) {

	int nTree = 1;
	//	Parameters from Queiroz Ph.D. Thesis (page 96)
	point *xi = new point[nTree];
	xi[0] = {4.49,0.0,0.0};	//	in cm
	double *ri = new double[nTree];
	ri[0] = 0.2257;		//	in cm
	double *qi = new double[nTree];
	qi[0] = 8.7;			//	in cm^3/s
	GeneratorData *instanceData = new GeneratorData(16000, 500, 0.9, 1.5, 8, 0.25, 7);

	SimpleDomain2D *domain = new SimpleDomain2D(inputFolder + "rectangular.vtk", 10000l, 5, instanceData);
	domain->setMinBifurcationAngle(M_PI * 3. / 18.);
	domain->setIsConvexDomain(true);
	StagedDomain *stagedDomain = new StagedDomain();
	stagedDomain->addStage(terminals, domain);
	cout << "Trying to generate the tree..." << endl;

	AbstractConstraintFunction<double, int> *gam = new ConstantConstraintFunction<double, int>(3);
	AbstractConstraintFunction<double, int> *epsLim = new ConstantConstraintFunction<double, int>(0.7);
	AbstractConstraintFunction<double, int> *nu = new ConstantConstraintFunction<double, int>(3.6);

	StagedFRROTreeGenerator *treeGenerator = new StagedFRROTreeGenerator(
			stagedDomain, xi[0], ri[0], qi[0], terminals,
			{ gam },
			{ epsLim },
			{ nu },
			0E0, 1E-6);
	cout << "Constructor executed." << endl;

	SingleVesselCCOOTree *tree = (SingleVesselCCOOTree *) treeGenerator->generate(10000,outputFolder);
	tree->save(outputFolder + "SG_Parameters.cco");
	VTKObjectTreeNodalWriter *treeWriter = new VTKObjectTreeNodalWriter();
	treeWriter->write(outputFolder + "SG_Parameters.vtp", tree);
	cout << "File successfully written." << endl;
}

void stagedGrowingExampleParameters3Stages(int terminals, string savepath) {

	int nTree = 1;
	//	Parameters from Queiroz Ph.D. Thesis (page 96)
	point *xi = new point[nTree];
	xi[0] = {4.49,0.0,0.0};	//	in cm
	double *ri = new double[nTree];
	ri[0] = 0.2257;		//	in cm
	double *qi = new double[nTree];
	qi[0] = 8.7;			//	in cm^3/s
	GeneratorData *instanceData1 = new GeneratorData(16000, 500, 0.9, 1.5, 8, 0.25, 7);
	GeneratorData *instanceData2 = new GeneratorData(16000, 500, 0.9, 1.5, 8, 0.25, 7);
	GeneratorData *instanceData3 = new GeneratorData(16000, 500, 0.9, 1.5, 8, 0.25, 7);

	SimpleDomain2D *domain1 = new SimpleDomain2D(inputFolder + "rectangular.vtk", 10000l, 5, instanceData1);
	domain1->setMinBifurcationAngle(M_PI * 3. / 18.);
	SimpleDomain2D *domain2 = new SimpleDomain2D(inputFolder + "rectangular.vtk", 10000l, 10, instanceData2);
	domain2->setMinBifurcationAngle(M_PI * 3. / 18.);
	SimpleDomain2D *domain3 = new SimpleDomain2D(inputFolder + "rectangular.vtk", 10000l, 25, instanceData3);
	domain3->setMinBifurcationAngle(M_PI * 3. / 18.);

	StagedDomain *stagedDomain = new StagedDomain();
	stagedDomain->addStage(50, domain1);
	stagedDomain->addStage(450, domain2);
	stagedDomain->addStage(1500, domain3);
	cout << "Trying to generate the tree..." << endl;

	AbstractConstraintFunction<double, int> *gam1 = new ConstantConstraintFunction<double, int>(3);
	AbstractConstraintFunction<double, int> *gam2 = new ConstantConstraintFunction<double, int>(3);
	AbstractConstraintFunction<double, int> *gam3 = new ConstantConstraintFunction<double, int>(3);
	AbstractConstraintFunction<double, int> *epsLim1 = new ConstantConstraintFunction<double, int>(0.7);
	AbstractConstraintFunction<double, int> *epsLim2 = new ConstantConstraintFunction<double, int>(0.5);
	AbstractConstraintFunction<double, int> *epsLim3 = new ConstantConstraintFunction<double, int>(0.2);
	AbstractConstraintFunction<double, int> *nu = new ConstantConstraintFunction<double, int>(3.6); //	Viscosity in cP -> MKS would be 0.0036

	StagedFRROTreeGenerator *treeGenerator = new StagedFRROTreeGenerator(
			stagedDomain, xi[0], ri[0], qi[0], terminals,
			{ gam1, gam2, gam3 },
			{ epsLim1, epsLim2, epsLim3 },
			{ nu, nu, nu },
			0E0, 1E-6);
	cout << "Constructor executed." << endl;

	treeGenerator->setSavingTasks({new VisualizationSavingTask(savepath,"SG_Parameters_S3_" + to_string(terminals)+"_step_")});

	SingleVesselCCOOTree *tree = (SingleVesselCCOOTree *) treeGenerator->generate(1,outputFolder);
	tree->save(outputFolder + "SG_Parameters_S3.cco");
	VTKObjectTreeNodalWriter *treeWriter = new VTKObjectTreeNodalWriter();
	treeWriter->write(outputFolder + "SG_Parameters_S3.vtp", tree);
	cout << "File successfully written." << endl;
}

void stagedGrowingExampleSubRegions2Stages(int terminals, string savepath) {

	int nTree = 1;
	//	Parameters from Queiroz Ph.D. Thesis (page 96)
	point *xi = new point[nTree];
	xi[0] = {4.8,0.0,0.0};	//	in cm
	double *ri = new double[nTree];
	ri[0] = 0.2257;		//	in cm
	double *qi = new double[nTree];
	qi[0] = 8.7;			//	in cm^3/s
	GeneratorData *instanceData1 = new GeneratorData(16000, 500, 0.9, 1, 8, 0.25, 7);
	GeneratorData *instanceData2 = new GeneratorData(16000, 500, 0.9, 1, 8, 0.25, 7);
	instanceData2->resetsDLim = true;

	DomainNVR *domain1 = new DomainNVR(inputFolder + "outterCircle.vtk", { inputFolder + "innerCircle4.vtk" }, 10000l, 5, instanceData1);
	domain1->setMinBifurcationAngle(M_PI * 3. / 18.);
	SimpleDomain *domain2 = new SimpleDomain(inputFolder + "outterCircle.vtk", 10000l, 10, instanceData2);
	domain2->setMinBifurcationAngle(M_PI * 1. / 18.);
	domain2->setIsConvexDomain(true);

	StagedDomain *stagedDomain = new StagedDomain();
	stagedDomain->addStage(50, domain1);
	stagedDomain->addStage(1950, domain2);
	cout << "Trying to generate the tree..." << endl;

	AbstractConstraintFunction<double, int> *gam1 = new ConstantConstraintFunction<double, int>(3);
	AbstractConstraintFunction<double, int> *gam2 = new ConstantConstraintFunction<double, int>(3);
	AbstractConstraintFunction<double, int> *epsLim1 = new ConstantConstraintFunction<double, int>(0.0);
	AbstractConstraintFunction<double, int> *epsLim2 = new ConstantConstraintFunction<double, int>(0.0);
	AbstractConstraintFunction<double, int> *nu = new ConstantConstraintFunction<double, int>(3.6); //	Viscosity in cP -> MKS would be 0.0036

	StagedFRROTreeGenerator *treeGenerator = new StagedFRROTreeGenerator(
			stagedDomain, xi[0], ri[0], qi[0], terminals,
			{ gam1, gam2 },
			{ epsLim1, epsLim2 },
			{ nu, nu },
			0E0, 1E-6);
	cout << "Constructor executed." << endl;

	treeGenerator->setSavingTasks({new VisualizationSavingTask(savepath,"SG_SubRegions_S2_" + to_string(terminals)+"_step_")});

	SingleVesselCCOOTree *tree = (SingleVesselCCOOTree *) treeGenerator->generate(1,outputFolder);
	tree->save(outputFolder + "SG_SubRegions_S2_2000.cco");
	VTKObjectTreeNodalWriter *treeWriter = new VTKObjectTreeNodalWriter();
	treeWriter->write(outputFolder + "SG_SubRegions_S2_2000.vtp", tree);
	cout << "File successfully written." << endl;
}

void stagedGrowingExampleDisjointRegions2Stages(int terminals) {

	int nTree = 1;
	//	Parameters from Queiroz Ph.D. Thesis (page 96)
	double *qi = new double[nTree];
	qi[0] = 8.7;			//	in cm^3/s
	GeneratorData *instanceData1 = new GeneratorData(16000, 500, 0.9, 1.5, 8, 0.25, 7);
	GeneratorData *instanceData2 = new GeneratorData(16000, 500, 0.9, 1.5, 8, 0.25, 7, 0, true);

	SimpleDomain *domain1 = new SimpleDomain(inputFolder + "circle_T1.vtk", 10000l, 5, instanceData1);
	domain1->setMinBifurcationAngle(M_PI * 3. / 18.);
	SimpleDomain *domain2 = new SimpleDomain(inputFolder + "circle_T2.vtk", 10000l, 10, instanceData2);
	domain2->setMinBifurcationAngle(M_PI * 3. / 18.);

	StagedDomain *stagedDomain = new StagedDomain();
	stagedDomain->addStage(800, domain1);
	stagedDomain->addStage(1200, domain2);
	cout << "Trying to generate the tree..." << endl;

	AbstractConstraintFunction<double, int> *gam1 = new ConstantConstraintFunction<double, int>(3);
	AbstractConstraintFunction<double, int> *gam2 = new ConstantConstraintFunction<double, int>(3);
	AbstractConstraintFunction<double, int> *epsLim1 = new ConstantConstraintFunction<double, int>(0.0);
	AbstractConstraintFunction<double, int> *epsLim2 = new ConstantConstraintFunction<double, int>(0.0);
	AbstractConstraintFunction<double, int> *nu = new ConstantConstraintFunction<double, int>(3.6); //	Viscosity in cP -> MKS would be 0.0036

	SingleVesselCCOOTree *tree = new SingleVesselCCOOTree(inputFolder + "SG_DisjointRegions_input.CCO",
			instanceData1, qi[0], gam1, epsLim1, nu, 0.0, 1E-6);


	cout << "Generating vasculature..." << endl;

	StagedFRROTreeGenerator * treeGenerator = new StagedFRROTreeGenerator(
			stagedDomain, tree, terminals,
			{ gam1, gam2 },
			{ epsLim1, epsLim2 },
			{ nu, nu });

	cout << "Constructor executed." << endl;

	tree = (SingleVesselCCOOTree *) treeGenerator->resume(1,outputFolder);
	tree->save(outputFolder + "SG_DisjointRegions_S2_2000.cco");
	VTKObjectTreeNodalWriter *treeWriter = new VTKObjectTreeNodalWriter();
	treeWriter->write(outputFolder + "SG_DisjointRegions_S2_2000.vtp", tree);
	cout << "File successfully written." << endl;
}

void multicriteriaGrowingExample1(int terminals, int dimensions) {

	int nTree = 1;
	//	Parameters from Queiroz Ph.D. Thesis (page 96)
	double *qi = new double[nTree];
	qi[0] = 10;			//	in cm^3/s
	GeneratorData *instanceData1 = new GeneratorData(16000, 500, 0.9, 1.0, 8, 0.25, 7, 0, true, new VolumetricCostEstimator());
	string label = "Vol";

	SimpleDomain *domain;
	if (dimensions == 2) {
		//	2D
		domain = new SimpleDomain(inputFolder + "circle_T3.vtk", 10000l, 5, instanceData1);
		label += "2D";
	}
	else {
		//	3D
		domain = new SimpleDomain(inputFolder + "sphere_T1.vtk", 10000l, 5, instanceData1);
		label += "3D";
	}
	domain->setMinBifurcationAngle(M_PI * 3. / 18.);
	domain->setIsConvexDomain(true);
	cout << "Trying to generate the tree..." << endl;
	StagedDomain *stagedDomain = new StagedDomain();
	stagedDomain->addStage(terminals, domain);

	AbstractConstraintFunction<double, int> *gam = new ConstantConstraintFunction<double, int>(3);
	AbstractConstraintFunction<double, int> *epsLim = new ConstantConstraintFunction<double, int>(0.0);
	AbstractConstraintFunction<double, int> *nu = new ConstantConstraintFunction<double, int>(0.036); //	Viscosity in cP -> MKS would be 0.0036

	SingleVesselCCOOTree *tree = new SingleVesselCCOOTree(inputFolder + "MG_ConcurrentTrees.CCO",
			instanceData1, qi[0], gam, epsLim, nu, 0.0, 1E-6);
	tree->setIsInCm(true);

	cout << "Generating vasculature..." << endl;

	StagedFRROTreeGenerator * treeGenerator = new StagedFRROTreeGenerator(
			stagedDomain, tree, terminals,
			{ gam },
			{ epsLim },
			{ nu });

	treeGenerator->setSavingTasks({new VisualizationSavingTask(outputFolder,label + to_string(terminals)+"_step_")});

	cout << "Constructor executed." << endl;

	tree = (SingleVesselCCOOTree *) treeGenerator->resume(100,outputFolder);
	tree->save(outputFolder + "MG_ConcurrentTrees_" + label + "_" + to_string(terminals) + ".cco");
	VTKObjectTreeNodalWriter *treeWriter = new VTKObjectTreeNodalWriter();
	treeWriter->write(outputFolder + "MG_ConcurrentTrees_" + label + "_" + to_string(terminals) + ".vtp", tree);
	cout << "File successfully written." << endl;

}

void multicriteriaGrowingExample2(int terminals, int dimensions) {

	int nTree = 1;
	//	Parameters from Queiroz Ph.D. Thesis (page 96)
	double *qi = new double[nTree];
	qi[0] = 10;			//	in cm^3/s
	string label = "HalfDeg";

	GeneratorData *instanceData0 = new GeneratorData(16000, 500, 0.9, 1.0, 8, 0.25, 7, 0, true, new VolumetricCostEstimator());
	GeneratorData *instanceData1;
	SimpleDomain *domain;
	if (dimensions == 2) {
		//	2D
		SimpleDomain *domain0 = new SimpleDomain(inputFolder + "circle_T3.vtk", 10000l, 5, instanceData0);
		instanceData1 = new GeneratorData(16000, 500, 0.9, 1.0, 8, 0.25, 7, 0, true,
				new AdimSproutingVolumetricCostEstimator(0.5E2, 0.5, 1.0, domain0->getSize(), 0.05));
		domain = new SimpleDomain(inputFolder + "circle_T3.vtk", 10000l, 5, instanceData1);
		label += "2D";
	}
	else {
		//	3D
		SimpleDomain *domain0 = new SimpleDomain(inputFolder + "sphere_T1.vtk", 10000l, 5, instanceData0);
		instanceData1 = new GeneratorData(16000, 500, 0.9, 1.0, 8, 0.25, 7, 0, true,
				new AdimSproutingVolumetricCostEstimator(1.0E4, 0.5, 1.0, domain0->getSize(), 0.05));
		domain = new SimpleDomain(inputFolder + "sphere_T1.vtk", 10000l, 5, instanceData1);
		label += "3D";
	}
	domain->setIsConvexDomain(true);
	domain->setMinBifurcationAngle(M_PI * 3. / 18.);
	cout << "Trying to generate the tree..." << endl;
	StagedDomain *stagedDomain = new StagedDomain();
	stagedDomain->addStage(terminals, domain);

	AbstractConstraintFunction<double, int> *gam = new ConstantConstraintFunction<double, int>(3);
	AbstractConstraintFunction<double, int> *epsLim = new ConstantConstraintFunction<double, int>(0.0);
	AbstractConstraintFunction<double, int> *nu = new ConstantConstraintFunction<double, int>(3.6); //	Viscosity in cP -> MKS would be 0.0036

	SingleVesselCCOOTree *tree = new SingleVesselCCOOTree(inputFolder + "MG_ConcurrentTrees.CCO",
			instanceData1, qi[0], gam, epsLim, nu, 0.0, 1E-6);
	tree->setIsInCm(true);

	cout << "Generating vasculature..." << endl;

	StagedFRROTreeGenerator * treeGenerator = new StagedFRROTreeGenerator(
			stagedDomain, tree, terminals,
			{ gam },
			{ epsLim },
			{ nu });

	treeGenerator->setSavingTasks({new VisualizationSavingTask(outputFolder,label + to_string(terminals)+"_step_")});
	treeGenerator->enableConfigurationFile(outputFolder + label + ".cfg");

	cout << "Constructor executed." << endl;

	tree = (SingleVesselCCOOTree *) treeGenerator->resume(100,outputFolder);
	tree->save(outputFolder + "MG2_ConcurrentTrees_" + label + to_string(terminals) + ".cco");
	VTKObjectTreeNodalWriter *treeWriter = new VTKObjectTreeNodalWriter();
	treeWriter->write(outputFolder + "MG2_ConcurrentTrees_" + label + "_" + to_string(terminals) + ".vtp", tree);
	cout << "File successfully written." << endl;

}

void multicriteriaGrowingExample3(int terminals, int dimensions) {

	int nTree = 1;
	//	Parameters from Queiroz Ph.D. Thesis (page 96)
	double *qi = new double[nTree];
	qi[0] = 10;			//	in cm^3/s
	string label = "2St_HalfDeg";

	GeneratorData *instanceData0 = new GeneratorData(16000, 500, 0.9, 1.0, 8, 0.25, 7, 0, true, new VolumetricCostEstimator());
	GeneratorData *instanceData1;
	GeneratorData *instanceData2;
	SimpleDomain *domain1;
	SimpleDomain *domain2;
	if (dimensions == 2) {
		//	2D
		SimpleDomain *domain0 = new SimpleDomain(inputFolder + "circle_T3.vtk", 10000l, 5, instanceData0);
		instanceData1 = new GeneratorData(16000, 500, 0.9, 1.0, 8, 0.25, 7, 0, true,
				new AdimSproutingVolumetricCostEstimator(0.5E2, 0.5, 1.0, domain0->getSize(), 0.05));
		domain1 = new SimpleDomain(inputFolder + "circle_T3.vtk", 10000l, 5, instanceData1);
		instanceData2 = new GeneratorData(16000, 500, 0.9, 1.0, 8, 0.25, 7, 0, true);
		domain2 = new SimpleDomain(inputFolder + "circle_T3.vtk", 10000l, 6, instanceData2);
		label += "2D";
	}
	else {
		//	3D
		SimpleDomain *domain0 = new SimpleDomain(inputFolder + "sphere_T1.vtk", 10000l, 5, instanceData0);
		instanceData1 = new GeneratorData(16000, 500, 0.9, 1.0, 8, 0.25, 7, 0, true,
				new AdimSproutingVolumetricCostEstimator(1.0E4, 0.5, 1.0, domain0->getSize(), 0.05));
		domain1 = new SimpleDomain(inputFolder + "sphere_T1.vtk", 10000l, 5, instanceData1);
		instanceData2 = new GeneratorData(16000, 500, 0.9, 1.0, 8, 0.25, 7, 0, true);
		domain2 = new SimpleDomain(inputFolder + "sphere_T1.vtk", 10000l, 6, instanceData2);
		label += "3D";
	}
	domain1->setIsConvexDomain(true);
	domain2->setIsConvexDomain(true);
	domain1->setMinBifurcationAngle(M_PI * 3. / 18.);
	domain2->setMinBifurcationAngle(M_PI * 3. / 18.);
	cout << "Trying to generate the tree..." << endl;
	StagedDomain *stagedDomain = new StagedDomain();
	stagedDomain->addStage(terminals * 0.025, domain1);
	stagedDomain->addStage(terminals * 0.975, domain2);

	AbstractConstraintFunction<double, int> *gam = new ConstantConstraintFunction<double, int>(3);
	AbstractConstraintFunction<double, int> *epsLim = new ConstantConstraintFunction<double, int>(0.0);
	AbstractConstraintFunction<double, int> *nu = new ConstantConstraintFunction<double, int>(3.6); //	Viscosity in cP -> MKS would be 0.0036

	SingleVesselCCOOTree *tree = new SingleVesselCCOOTree(inputFolder + "MG_ConcurrentTrees.CCO",
			instanceData1, qi[0], gam, epsLim, nu, 0.0, 1E-6);
	tree->setIsInCm(true);

	cout << "Generating vasculature..." << endl;

	StagedFRROTreeGenerator * treeGenerator = new StagedFRROTreeGenerator(
			stagedDomain, tree, terminals,
			{ gam, gam },
			{ epsLim, epsLim },
			{ nu, nu });

	treeGenerator->setSavingTasks({new VisualizationSavingTask(outputFolder,label + to_string(terminals)+"_step_")});
	treeGenerator->enableConfigurationFile(outputFolder + label + ".cfg");

	cout << "Constructor executed." << endl;

	tree = (SingleVesselCCOOTree *) treeGenerator->resume(100,outputFolder);
	tree->save(outputFolder + "MG2_ConcurrentTrees_" + label + to_string(terminals) + ".cco");
	VTKObjectTreeNodalWriter *treeWriter = new VTKObjectTreeNodalWriter();
	treeWriter->write(outputFolder + "MG2_ConcurrentTrees_" + label + "_" + to_string(terminals) + ".vtp", tree);
	cout << "File successfully written." << endl;

}

void flowDistributionExample1(int terminals) {
	//	Parameters from Queiroz Ph.D. Thesis (page 96)
	point xi = {0.975,0.0,0.0};	//	in cm
	double ri = 0.03;		//	in cm
	double qi = 10;			//	in cm^3/s

	string label = "Uniform";

	GeneratorData *instanceData = new GeneratorData(16000, 500, 0.9, 1.0, 8, 0.25, 7, 0, true);
	SimpleDomain *domain = new SimpleDomain(inputFolder + "circle_T4.vtk", 10000l, 6, instanceData,new UniformDistributionGenerator());
	domain->setMinBifurcationAngle(M_PI * 2. / 18.);

	StagedDomain *stagedDomain = new StagedDomain();
	stagedDomain->addStage(terminals, domain);

	cout << "Trying to generate the tree..." << endl;

	AbstractConstraintFunction<double, int> *gam = new ConstantConstraintFunction<double, int>(3);
	AbstractConstraintFunction<double, int> *epsLim = new ConstantConstraintFunction<double, int>(0.0);
	AbstractConstraintFunction<double, int> *nu = new ConstantConstraintFunction<double, int>(3.6); //	Viscosity in cP -> MKS would be 0.0036

	cout << "Generating vasculature..." << endl;

	StagedFRROTreeGenerator *treeGenerator = new StagedFRROTreeGenerator(
			stagedDomain, xi, ri, qi, terminals,
			{ gam },
			{ epsLim },
			{ nu },
			0E0, 1E-6);
	string filePrefix = outputFolder + "FD_ConcurrentTrees_" + label + to_string(terminals);
	treeGenerator->enableConfigurationFile(filePrefix + ".cfg");
	cout << "Constructor executed." << endl;

	SingleVesselCCOOTree *tree = (SingleVesselCCOOTree *) treeGenerator->generate(50,outputFolder);
	tree->save(filePrefix + ".cco");
	VTKObjectTreeNodalWriter *treeWriter = new VTKObjectTreeNodalWriter();
	treeWriter->write(filePrefix + ".vtp", tree);
	cout << "File successfully written." << endl;
}

void flowDistributionExample2(int terminals) {
	//	Parameters from Queiroz Ph.D. Thesis (page 96)
	point xi = {0.975,0.0,0.0};	//	in cm
	double ri = 0.03;		//	in cm
	double qi = 10;			//	in cm^3/s

	string label = "Normal";

	GeneratorData *instanceData = new GeneratorData(16000, 500, 0.9, 1.0, 8, 0.25, 7, 0, true);
	SimpleDomain *domain = new SimpleDomain(inputFolder + "circle_T4.vtk", 10000l, 6, instanceData, new NormalDistributionGenerator({0.0,0.0,0.0},{0.25,0.0001,0.25}) );
	domain->setMinBifurcationAngle(M_PI * 2. / 18.);

	StagedDomain *stagedDomain = new StagedDomain();
	stagedDomain->addStage(terminals, domain);

	cout << "Trying to generate the tree..." << endl;

	AbstractConstraintFunction<double, int> *gam = new ConstantConstraintFunction<double, int>(3);
	AbstractConstraintFunction<double, int> *epsLim = new ConstantConstraintFunction<double, int>(0.0);
	AbstractConstraintFunction<double, int> *nu = new ConstantConstraintFunction<double, int>(3.6); //	Viscosity in cP -> MKS would be 0.0036

	cout << "Generating vasculature..." << endl;

	StagedFRROTreeGenerator *treeGenerator = new StagedFRROTreeGenerator(
			stagedDomain, xi, ri, qi, terminals,
			{ gam },
			{ epsLim },
			{ nu },
			0E0, 1E-6);
	string filePrefix = outputFolder + "FD_ConcurrentTrees_" + label + to_string(terminals);
	treeGenerator->enableConfigurationFile(filePrefix + ".cfg");
	cout << "Constructor executed." << endl;

	SingleVesselCCOOTree *tree = (SingleVesselCCOOTree *) treeGenerator->generate(50,outputFolder);
	tree->save(filePrefix + ".cco");
	VTKObjectTreeNodalWriter *treeWriter = new VTKObjectTreeNodalWriter();
	treeWriter->write(filePrefix + ".vtp", tree);
	cout << "File successfully written." << endl;
}

void flowDistributionExample3(int terminals) {
	//	Parameters from Queiroz Ph.D. Thesis (page 96)
	point xi = {0.975,0.0,0.0};	//	in cm
	double ri = 0.03;		//	in cm
	double qi = 10;			//	in cm^3/s

	string label = "2_Normal";

	GeneratorData *instanceData = new GeneratorData(16000, 500, 0.9, 1.0, 8, 0.25, 7, 0, true);
	SimpleDomain *domain = new SimpleDomain(inputFolder + "circle_T4.vtk", 10000l, 6, instanceData, new CompositeDistributionGenerator({
			new NormalDistributionGenerator({0.0,0.0,0.5},{0.25,0.0001,0.25}),
			new NormalDistributionGenerator({0.0,0.0,-0.5},{0.25,0.0001,0.25})	}));
	domain->setMinBifurcationAngle(M_PI * 0. / 18.);

	StagedDomain *stagedDomain = new StagedDomain();
	stagedDomain->addStage(terminals, domain);

	cout << "Trying to generate the tree..." << endl;

	AbstractConstraintFunction<double, int> *gam = new ConstantConstraintFunction<double, int>(3);
	AbstractConstraintFunction<double, int> *epsLim = new ConstantConstraintFunction<double, int>(0.0);
	AbstractConstraintFunction<double, int> *nu = new ConstantConstraintFunction<double, int>(3.6); //	Viscosity in cP -> MKS would be 0.0036

	cout << "Generating vasculature..." << endl;

	StagedFRROTreeGenerator *treeGenerator = new StagedFRROTreeGenerator(
			stagedDomain, xi, ri, qi, terminals,
			{ gam },
			{ epsLim },
			{ nu },
			0E0, 1E-6);
	string filePrefix = outputFolder + "FD_ConcurrentTrees_" + label + to_string(terminals);
	treeGenerator->enableConfigurationFile(filePrefix + ".cfg");
	cout << "Constructor executed." << endl;

	SingleVesselCCOOTree *tree = (SingleVesselCCOOTree *) treeGenerator->generate(50,outputFolder);
	tree->save(filePrefix + ".cco");
	VTKObjectTreeNodalWriter *treeWriter = new VTKObjectTreeNodalWriter();
	treeWriter->write(filePrefix + ".vtp", tree);
	cout << "File successfully written." << endl;
}

void flowDistributionExample4(int terminals) {
	//	Parameters from Queiroz Ph.D. Thesis (page 96)
	point xi = {0.975,0.0,0.0};	//	in cm
	double ri = 0.03;		//	in cm
	double qi = 10;			//	in cm^3/s

	string label = "2_Normal_Dif";

	GeneratorData *instanceData = new GeneratorData(16000, 500, 0.9, 1.0, 8, 0.25, 7, 0, true);
	SimpleDomain *domain = new SimpleDomain(inputFolder + "circle_T4.vtk", 10000l, 6, instanceData, new CompositeDistributionGenerator({
			new NormalDistributionGenerator({0.0,0.0,0.5},{0.25,0.0001,0.25}),
			new NormalDistributionGenerator({0.0,0.0,-0.5},{0.15,0.0001,0.15})	}));
	domain->setMinBifurcationAngle(M_PI * 0. / 18.);

	StagedDomain *stagedDomain = new StagedDomain();
	stagedDomain->addStage(terminals, domain);

	cout << "Trying to generate the tree..." << endl;

	AbstractConstraintFunction<double, int> *gam = new ConstantConstraintFunction<double, int>(3);
	AbstractConstraintFunction<double, int> *epsLim = new ConstantConstraintFunction<double, int>(0.0);
	AbstractConstraintFunction<double, int> *nu = new ConstantConstraintFunction<double, int>(3.6); //	Viscosity in cP -> MKS would be 0.0036

	cout << "Generating vasculature..." << endl;

	StagedFRROTreeGenerator *treeGenerator = new StagedFRROTreeGenerator(
			stagedDomain, xi, ri, qi, terminals,
			{ gam },
			{ epsLim },
			{ nu },
			0E0, 1E-6);
	string filePrefix = outputFolder + "FD_ConcurrentTrees_" + label + to_string(terminals);
	treeGenerator->enableConfigurationFile(filePrefix + ".cfg");
	cout << "Constructor executed." << endl;

	SingleVesselCCOOTree *tree = (SingleVesselCCOOTree *) treeGenerator->generate(50,outputFolder);
	tree->save(filePrefix + ".cco");
	VTKObjectTreeNodalWriter *treeWriter = new VTKObjectTreeNodalWriter();
	treeWriter->write(filePrefix + ".vtp", tree);
	cout << "File successfully written." << endl;
}

void flowReservedExample1(int terminals, string ccoFile, string label) {

	int nTree = 1;
	//	Parameters from Queiroz Ph.D. Thesis (page 96)
	double *qi = new double[nTree];
	qi[0] = 10;			//	in cm^3/s

	GeneratorData *instanceData1 = new GeneratorData(16000, 500, 0.9, 1.5, 8, 0.25, 7);
	SimpleDomain *domain1 = new SimpleDomain(inputFolder + "sphere_T2.vtk", 10000l, 5, instanceData1);
	domain1->setMinBifurcationAngle(M_PI * 3. / 18.);

	StagedDomain *stagedDomain = new StagedDomain();
	stagedDomain->addStage(terminals, domain1);
	cout << "Trying to generate the tree..." << endl;

	AbstractConstraintFunction<double, int> *gam1 = new ConstantConstraintFunction<double, int>(3);
	AbstractConstraintFunction<double, int> *epsLim1 = new ConstantConstraintFunction<double, int>(0.0);
	AbstractConstraintFunction<double, int> *nu = new ConstantConstraintFunction<double, int>(3.6); //	Viscosity in cP -> MKS would be 0.0036

	SingleVesselCCOOTree *tree = new SingleVesselCCOOTree(inputFolder + ccoFile,
			instanceData1, qi[0], gam1, epsLim1, nu, 0.0, 1E-6);

	cout << "Generating vasculature..." << endl;

	StagedFRROTreeGenerator * treeGenerator = new StagedFRROTreeGenerator(
			stagedDomain, tree, terminals,
			{ gam1 },
			{ epsLim1 },
			{ nu, nu });

	string filePrefix = outputFolder + "FD_FlowReserved_" + label + to_string(terminals);
	treeGenerator->enableConfigurationFile(filePrefix + ".cfg");
	cout << "Constructor executed." << endl;

	tree = (SingleVesselCCOOTree *) treeGenerator->resume(1,outputFolder);
	tree->save(filePrefix + ".cco");
	VTKObjectTreeNodalWriter *treeWriter = new VTKObjectTreeNodalWriter();
	treeWriter->write(filePrefix + ".vtp", tree);
	cout << "File successfully written." << endl;
}

void standardCCO(int terminals, string savepath) {

	int nTree = 1;
	//	Parameters from Queiroz Ph.D. Thesis (page 96)
	point *xi = new point[nTree];
	xi[0] = {4.8,0.0,0.0};	//	in cm
	double *ri = new double[nTree];
	ri[0] = 0.2257;		//	in cm
	double *qi = new double[nTree];
	qi[0] = 8.7;			//	in cm^3/s
	GeneratorData *instanceData1 = new GeneratorData(16000, 500, 0.9, 1., 8, 0.25, 7);
	SimpleDomain *domain1 = new SimpleDomain(inputFolder + "outterCircle.vtk", 10000l, 10, instanceData1);
	domain1->setMinBifurcationAngle(M_PI * 3. / 18.);
//	domain1->setIsConvexDomain(true);

	StagedDomain *stagedDomain = new StagedDomain();
	stagedDomain->addStage(2000, domain1);
	cout << "Trying to generate the tree..." << endl;

	AbstractConstraintFunction<double, int> *gam1 = new ConstantConstraintFunction<double, int>(3);
	AbstractConstraintFunction<double, int> *epsLim1 = new ConstantConstraintFunction<double, int>(0.0);
	AbstractConstraintFunction<double, int> *nu = new ConstantConstraintFunction<double, int>(3.6); //	Viscosity in cP -> MKS would be 0.0036

	StagedFRROTreeGenerator *treeGenerator = new StagedFRROTreeGenerator(
			stagedDomain, xi[0], ri[0], qi[0], terminals,
			{ gam1},
			{ epsLim1},
			{ nu},
			0E0, 1E-6);
	cout << "Constructor executed." << endl;

	treeGenerator->setSavingTasks({new VisualizationSavingTask(savepath,"CCO_Standard_Circle_" + to_string(terminals)+"_step_")});

	SingleVesselCCOOTree *tree = (SingleVesselCCOOTree *) treeGenerator->generate(1,savepath);
	tree->save(outputFolder + "CCO_Standard_Circle_1000.cco");
	VTKObjectTreeNodalWriter *treeWriter = new VTKObjectTreeNodalWriter();
	treeWriter->write(outputFolder + "CCO_Standard_Circle_1000.vtp", tree);
	cout << "File successfully written." << endl;
}

void standardCCOAngle(int terminals, string savepath, double minAngle) {

	int nTree = 1;
	//	Parameters from Queiroz Ph.D. Thesis (page 96)
	point *xi = new point[nTree];
	xi[0] = {4.8,0.0,0.0};	//	in cm
	double *ri = new double[nTree];
	ri[0] = 0.2257;		//	in cm
	double *qi = new double[nTree];
	qi[0] = 8.7;			//	in cm^3/s
	GeneratorData *instanceData1 = new GeneratorData(16000, 500, 0.9, 1., 8, 0.25, 7);
	SimpleDomain *domain1 = new SimpleDomain(inputFolder + "outterCircle.vtk", 10000l, 10, instanceData1);
	domain1->setMinBifurcationAngle(M_PI * minAngle / 180.);
//	domain1->setIsConvexDomain(true);

	StagedDomain *stagedDomain = new StagedDomain();
	stagedDomain->addStage(2000, domain1);
	cout << "Trying to generate the tree..." << endl;

	AbstractConstraintFunction<double, int> *gam1 = new ConstantConstraintFunction<double, int>(3);
	AbstractConstraintFunction<double, int> *epsLim1 = new ConstantConstraintFunction<double, int>(0.0);
	AbstractConstraintFunction<double, int> *nu = new ConstantConstraintFunction<double, int>(3.6); //	Viscosity in cP -> MKS would be 0.0036

	StagedFRROTreeGenerator *treeGenerator = new StagedFRROTreeGenerator(
			stagedDomain, xi[0], ri[0], qi[0], terminals,
			{ gam1},
			{ epsLim1},
			{ nu},
			0E0, 1E-6);
	cout << "Constructor executed." << endl;

	string label = "CCO_Angle_" + to_string( (int)(round(minAngle)) )+ "_Circle_";
	treeGenerator->setSavingTasks({new VisualizationSavingTask(savepath,label + to_string(terminals)+"_step_")});

	SingleVesselCCOOTree *tree = (SingleVesselCCOOTree *) treeGenerator->generate(10000,savepath);
	tree->save(outputFolder + label + to_string(terminals) + ".cco");
	VTKObjectTreeNodalWriter *treeWriter = new VTKObjectTreeNodalWriter();
	treeWriter->write(outputFolder + label + to_string(terminals) + ".vtp", tree);
	cout << "File successfully written." << endl;
}

void standardCCOSymmetry(int terminals, string savepath, double symFactor) {

	int nTree = 1;
	//	Parameters from Queiroz Ph.D. Thesis (page 96)
	point *xi = new point[nTree];
	xi[0] = {4.8,0.0,0.0};	//	in cm
	double *ri = new double[nTree];
	ri[0] = 0.2257;		//	in cm
	double *qi = new double[nTree];
	qi[0] = 8.7;			//	in cm^3/s
	GeneratorData *instanceData1 = new GeneratorData(16000, 500, 0.9, 1., 8, 0.25, 7);
	SimpleDomain *domain1 = new SimpleDomain(inputFolder + "outterCircle.vtk", 10000l, 10, instanceData1);
	domain1->setMinBifurcationAngle(M_PI * 0.0 / 180.0);
//	domain1->setIsConvexDomain(true);

	StagedDomain *stagedDomain = new StagedDomain();
	stagedDomain->addStage(2000, domain1);
	cout << "Trying to generate the tree..." << endl;

	AbstractConstraintFunction<double, int> *gam1 = new ConstantConstraintFunction<double, int>(3);
	AbstractConstraintFunction<double, int> *epsLim1 = new ConstantConstraintFunction<double, int>(symFactor);
	AbstractConstraintFunction<double, int> *nu = new ConstantConstraintFunction<double, int>(3.6); //	Viscosity in cP -> MKS would be 0.0036

	StagedFRROTreeGenerator *treeGenerator = new StagedFRROTreeGenerator(
			stagedDomain, xi[0], ri[0], qi[0], terminals,
			{ gam1},
			{ epsLim1},
			{ nu},
			0E0, 1E-6);
	cout << "Constructor executed." << endl;

	string label = "CCO_Sym_025_Circle_";
	treeGenerator->setSavingTasks({new VisualizationSavingTask(savepath,label + to_string(terminals)+"_step_")});

	SingleVesselCCOOTree *tree = (SingleVesselCCOOTree *) treeGenerator->generate(10000,savepath);
	tree->save(outputFolder + label + to_string(terminals) + ".cco");
	VTKObjectTreeNodalWriter *treeWriter = new VTKObjectTreeNodalWriter();
	treeWriter->write(outputFolder + label + to_string(terminals) + ".vtp", tree);
	cout << "File successfully written." << endl;
}

int main() {

	//	Staged growing examples
//	CylinderCreator *creator2= new CylinderCreator({0.0,0.0,-2.0}, 0.8, 1E-3, 20);
//	creator2->create(inputFolder+"circle_T1.vtk");
//	CylinderCreator *creator = new CylinderCreator({-3.0,0.0,-2.0}, 1.0, 1E-3, 20);
//	creator->create(inputFolder+"circle_T2.vtk");

//	stagedGrowingExampleSubRegions2Stages(2000,outputFolder +"case1/");
//	stagedGrowingExampleParameters1Stage(2000);
//	stagedGrowingExampleParameters3Stages(2000,outputFolder +"case2/");
//	stagedGrowingExampleDisjointRegions2Stages(2000);

	//	Multi-criteria growing examples
//	SphereCreator *creator3= new SphereCreator({0.0,0.0,0.0}, 1.0, 10., 10.);
//	creator3->create(inputFolder+"sphere_T1.vtk");
//	CylinderCreator *creator4= new CylinderCreator({0.0,0.0,0.0}, 1.0, 1E-3, 20);
//	creator4->create(inputFolder+"circle_T3.vtk");

//	multicriteriaGrowingExample1(1000, 2);
//	multicriteriaGrowingExample1(1000, 3);
//	multicriteriaGrowingExample2(1000, 2);
//	multicriteriaGrowingExample2(1000, 3);
//	multicriteriaGrowingExample3(1000, 2);
//	multicriteriaGrowingExample3(1000, 3);

	//	Flow distribution
//	CylinderCreator *creator5 = new CylinderCreator( { 0.0, 0.0, 0.0 }, 1.0, 1E-3, 20);
//	creator5->create(inputFolder + "circle_T4.vtk");

//	flowDistributionExample1(1500);
	flowDistributionExample2(1500);
//	flowDistributionExample3(1500);
//	flowDistributionExample4(1500);

	//	Reserved flow
//	SphereCreator *creator6= new SphereCreator({0.0,0.0,0.0}, 1.0, 10., 10.);
//	creator6->create(inputFolder+"sphere_T2.vtk");
//
//	flowReservedExample1(200,"FD_reservedFlow_input_0.CCO","NoReserved");
//	flowReservedExample1(200,"FD_reservedFlow_input_05.CCO","05");
//	flowReservedExample1(200,"FD_reservedFlow_input_099.CCO","099");

	//	Standard CCO example
//	standardCCO(2000,outputFolder +"stdCCO/");
//	standardCCOAngle(1000,outputFolder +"case3/",0.0);
//	standardCCOAngle(1000,outputFolder +"case3/",50.0);
//	standardCCOSymmetry(1000,outputFolder +"case4/", 0.25);

	return 0;
}
